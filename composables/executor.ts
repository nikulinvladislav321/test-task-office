import { useLoading } from "./useLoading";
import { message } from "ant-design-vue";

export async function executor(
  url: string,
  options?: RequestInit
): Promise<any> {
  try {
    const response = await fetch(url, options);
    if (!response.ok) {
      message.error("Ошибка при загрузке данных:", response.statusText);
    }
    return await response.json();
  } catch (error) {
    console.error("Ошибка запроса:", error);
    message.error("Ошибка при выполнении запроса:", error);
  }
}
