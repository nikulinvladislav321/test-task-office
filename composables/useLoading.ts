import { ref, Ref } from "vue";

export function useLoading(): {
  isLoading: Ref<boolean>;
  startLoading: () => void;
  stopLoading: () => void;
} {
  const isLoading = ref(false);

  function startLoading() {
    isLoading.value = true;
  }

  function stopLoading() {
    isLoading.value = false;
  }

  return { isLoading, startLoading, stopLoading };
}
