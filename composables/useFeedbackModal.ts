import type { UploadChangeParam } from "ant-design-vue";
import { message } from "ant-design-vue";
import type { Feedbackitem } from "~/types";
import DOMPurify from "dompurify";

export const handleChange = (info: UploadChangeParam) => {
  const status = info.file.status;
  if (status !== "uploading") {
    console.log(info.file, info.fileList);
  }
  if (status === "done") {
    message.success(`${info.file.name} file uploaded successfully.`);
  } else if (status === "error") {
    message.error(`${info.file.name} file upload failed.`);
  }
};

export const cleanForm = (form: Feedbackitem) => {
  Object.keys(form).forEach((key) => {
    if (typeof form[key] === "string") {
      form[key] = DOMPurify.sanitize(form[key]);
    }
  });
  return form;
};

export const handleBeforeUpload = (file: File) => {
  const isLt5M = file.size / 1024 / 1024 < 5;
  if (!isLt5M) {
    message.error("Размер файла превышает 5 МБ");
  }
  return isLt5M;
};

export const clearForm = (form: Feedbackitem) => {
  form.name = "";
  form.email = "";
  form.city = "";
  form.phone = "";
  form.message = "";
  form.type = "";
  form.file = [];
  form.id = null;
  return form;
};

export const generateUniqueId = () => {
  return "id-" + Math.random().toString(36).substr(2, 9);
};
