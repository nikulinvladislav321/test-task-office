export interface Feedbackitem {
  name: string;
  email: string;
  phone: string;
  message: string;
  city: string;
  type: string;
  file: File[];
  id?: string | null;
}
