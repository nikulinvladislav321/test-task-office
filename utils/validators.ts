import type { Rule } from "ant-design-vue/es/form";

export const vvRequired: Rule = {
  required: true,
  message: "Поле не может быть пустым",
};

export const vvPhone: Rule = {
  required: true,
  len: 18,
  message: "Телефон должен содержать 11 цифр",
};

export const vvEmail: Rule = {
  required: true,
  type: "email",
  message: "E-mail введен некорректно",
};

export const formatPhoneNumber = (phone: string) => {
  // Удаляем все, кроме цифр и знака +
  let cleaned = phone.replace(/[^\d+]/g, "");

  // Обработка первого символа
  if (cleaned.startsWith("8")) {
    cleaned = "+7" + cleaned.substring(1);
  } else if (cleaned.startsWith("7")) {
    cleaned = "+" + cleaned;
  }

  // Удаление лишних символов
  cleaned = cleaned.substring(0, 12);

  // Форматирование номера
  let formatted = cleaned
    .replace(/^[+]7/, "") // Убираем +7 для форматирования
    .replace(/(\d{3})(\d{0,3})(\d{0,2})(\d{0,2})/, "($1) $2 $3 $4")
    .trim();

  // Добавляем +7 обратно
  return "+7 " + formatted;
};
