import type { Feedbackitem } from "~/types";
import { message } from "ant-design-vue";
import { executor } from "~/composables/executor";

interface FeedbackState {
  feedbacks: Feedbackitem[];
  isLoading: boolean;
  currentPage: number;
  pageSize: number;
  totalItems: number;
}

export const useFeedbackStore = defineStore("feedback", {
  state: (): FeedbackState => ({
    feedbacks: [],
    isLoading: false,
    currentPage: 1,
    pageSize: 10,
    totalItems: 0,
  }),
  getters: {
    getFeedbacks: (state) => state.feedbacks,
    getCurrentPage: (state) => state.currentPage,
  },
  actions: {
    async fetchData() {
      this.isLoading = true;
      try {
        const response = await executor(
          `/api/feedback?page=${this.currentPage}&limit=${this.pageSize}`
        );
        this.feedbacks = response.data;
        this.totalItems = response.total; // Предполагаем, что сервер возвращает общее количество элементов
      } catch (error) {
        message.error("Ошибка при выполнении запроса:", error);
      } finally {
        this.isLoading = false;
      }
    },

    // Метод для установки текущей страницы
    setCurrentPage(page: number) {
      this.currentPage = page;
    },

    // Метод для установки размера страницы
    setPageSize(size: number) {
      this.pageSize = size;
    },
    setTotalItems(total: number) {
      this.totalItems = total;
    },
  },
});
