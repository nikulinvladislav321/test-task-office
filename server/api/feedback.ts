import { feedbackItems } from "../mock";
import { useQuery } from "nuxt3";

// export default defineEventHandler(() => {
//   return feedbackItems;
// });

// import { mockData } from './mockData';

export default defineEventHandler((event) => {
  const url = new URL(event.req.url, `http://${event.req.headers.host}`);
  const pageParam = url.searchParams.get("page");
  const limitParam = url.searchParams.get("limit");

  const page = parseInt(pageParam) || 1;
  const limit = parseInt(limitParam) || 10;

  const startIndex = (page - 1) * limit;
  const endIndex = startIndex + limit;

  const paginatedData = feedbackItems.slice(startIndex, endIndex);

  return {
    page,
    limit,
    total: feedbackItems.length,
    data: paginatedData,
  };
});
