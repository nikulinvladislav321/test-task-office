// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  css: ["assets/scss/global.scss"],
  devtools: { enabled: true },
  // vite: true,
  modules: [
    "@nuxt/image",
    "nuxt-icon",
    "@ant-design-vue/nuxt",
    "@pinia/nuxt",
    [
      "@nuxtjs/google-fonts",
      {
        families: {
          Roboto: true,
        },
      },
    ],
  ],
  pinia: {
    storesDirs: ["./store/**"],
  },
});
